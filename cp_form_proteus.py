#!/usr/bin/env python3
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import csv
import os
import sys
from argparse import ArgumentParser
from decimal import Decimal


try:
    from proteus import Model, config
except ImportError:
    prog = os.path.basename(sys.argv[0])
    sys.exit("proteus must be installed to use %s" % prog)


def update_cp_forms():
    CpForm = Model.get('farming.cp_document_number')
    SupplierCertificate = Model.get('farming.supplier_certificate')
    supplier_certificates = SupplierCertificate.find([])
    for supplier_certificate in supplier_certificates:
        for cp_form in supplier_certificate.cp_forms:
            precision = Decimal('0.0001')
            number = Decimal(calculate_amount(supplier_certificate.invoice_lines, cp_form.supplier.id))
            rounded_number = number.quantize(precision)
            cp_form.amount = rounded_number
            # cp_form.amount = Decimal(calculate_amount(supplier_certificate.invoice_lines, cp_form.supplier.id))
            cp_form.save()
    print("finzalido")


def calculate_amount(invoice_lines, supplier):
    amount = Decimal(0)
    for il in invoice_lines:
        if il.invoice.party.id == supplier:
            amount += il.unit_price * Decimal(il.quantity)
    print(amount)
    return amount


def main(database, config_file=None):
    config.set_trytond(database, config_file=config_file)
    with config.get_config().set_context(active_test=False):
        do_import()


def do_import():
    update_cp_forms()


def run():
    parser = ArgumentParser()
    parser.add_argument('-d', '--database', dest='database', required=True)
    parser.add_argument('-c', '--config', dest='config_file',
        help='the trytond config file')

    args = parser.parse_args()
    main(args.database, args.config_file)


if __name__ == '__main__':
    run()


# trytond -v -c ~/.trytond/trytond.conf