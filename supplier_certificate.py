from trytond.model import fields, ModelSQL, ModelView, Unique, Workflow
from trytond.pool import Pool
# from trytond.transaction import Transaction
# from trytond.wizard import Button, StateTransition, StateView, Wizard
from trytond.pyson import Eval
from decimal import Decimal
from trytond.exceptions import UserError
from lxml import etree
from datetime import datetime
from datetime import date
from trytond.transaction import Transaction
from trytond.report import Report
from trytond.wizard import Button, StateReport, StateTransition, StateView, Wizard


STATES = {
    'readonly': Eval('state') != 'draft'
}


class SupplierCertificate(Workflow, ModelSQL, ModelView):
    """Supplier Certificate"""
    __name__ = 'farming.supplier_certificate'

    number = fields.Char('Number', states=STATES)
    cp_forms = fields.One2Many('farming.cp_document_number', 'certificate', 'Forms',
                               states= {
                                   'readonly': Eval('state') == 'done',
                                   'required': Eval('state') == 'done'
                                   })
    # supplier = fields.Many2Many('party.party', 'Supplier')
    form_date = fields.Date('Fecha de Formulario')
    suppliers = fields.Many2Many('farming.supplier_certificate-party.party', 'supplier_certificate',
                                 'party', 'Party', states=STATES)
    provider_link = fields.Char('Link Mail', states=STATES)
    start_date = fields.Date('Start Date', states=STATES)
    end_date = fields.Date('End Date', states=STATES)
    invoice_lines = fields.Many2Many('farming.supplier_certificate-account_invoice_line', 'supplier_certificate',
                            'invoice_line', 'Invoice Lines', domain=[
                                        ('invoice.type', '=', 'in'),
                                        ('invoice.party', 'in', Eval('suppliers')),
                                        ('product.exportable', '=', True),
                                        ('invoice.invoice_date', '>=', Eval('start_date')),
                                        ('invoice.invoice_date', '<=', Eval('end_date')),
                                        ], depends=['suppliers', 'start_date', 'end_date'],
                                        states=STATES)
                                        # ('invoice', '=', Eval('invoice')),
    state = fields.Selection([
            ('draft', 'Draft'),
            ('cancelled', 'Cancelled'),
            ('done', 'Done'),
            ('processed', 'Processed'),
        ], 'State', readonly=True)
    total_amount = fields.Function(fields.Numeric('Total_amount'), 'get_total_amount')
    vat_amount = fields.Function(fields.Numeric('Vat Amount'), 'get_vat_amount')
    xml_certificate = fields.Text('XML Certificate')
    dexes = fields.One2Many('farming.supplier_certificate_dex',
                            'supplier_certificate', 'Dexs',
                            states={'readonly': Eval('state') in ['done', 'draft', 'cancelled']})
    total_documents = fields.Function(fields.Integer('Total Documents'), 'get_total_documents')

    @classmethod
    def __setup__(cls):
        super(SupplierCertificate, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'processed'),
            ('processed', 'draft'),
            ('processed', 'done'),
            ('processed', 'cancelled'),
            ('done', 'draft'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft'])
            },
            'cancel': {
                'invisible': Eval('state').in_(['cancelled', 'draft', 'done', 'processed']),
            },
            'process': {
                'invisible': Eval('state') != 'draft',
            },
            'done': {
                'invisible': Eval('state').in_(['draft', 'cancelled', 'done']),
            },
            'create_certificate': {
                'invisible': Eval('state').in_(['cancelled', 'done', 'processed']),
            },

        })

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('processed')
    def process(cls, records):
        pass
        # for record in records:
        #     record.set_number()

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        for record in records:
            for cp_form in record.cp_forms:
                if cp_form.cp_form == None:
                    raise UserError(f'{cp_form.supplier.name} no tiene número de formulario')

    def get_total_documents(self, name):
        total = 0
        if self.suppliers:
            total = len(self.suppliers)
        return total

    def get_total_amount(self, name):
        total_amount = 0
        if self.invoice_lines:
            for invoice_line in self.invoice_lines:
                total_amount += invoice_line.amount
        return Decimal(total_amount)

    def get_vat_amount(self, name):
        total_amount_taxes = 0
        TAX = Pool().get('account.tax')
        iva = TAX(2)
        if self.invoice_lines:
            for invoice_line in self.invoice_lines:
                # total_amount_taxes += invoice_line.amount * iva.rate * Decimal(invoice_line.quantity)
                total_amount_taxes += invoice_line.amount * iva.rate
        return Decimal(total_amount_taxes)

    @classmethod
    def calculate_month(cls):
        _month = date.today().month + 6
        if _month > 12:
            result = date.today().replace(month=_month - 12, year=date.today().year+1)
        else:
            result = date.today().replace(month=_month)
        return result

    @classmethod
    def group_invoices_by_party(cls, parties, invoice_lines):
        lines = invoice_lines
        parties_dict = {}
        for party in parties:
            parties_dict[party] = {
                'party': party,
                'invoices': {}
            }
            for line in lines:
                if line.invoice.party == party:
                    invoice_number = line.invoice.number
                    if invoice_number in parties_dict[party]['invoices']:
                        if line.id not in parties_dict[party]['invoices'][invoice_number]:
                            parties_dict[party]['invoices'][invoice_number][line.id] = line
                    else:
                        parties_dict[party]['invoices'][invoice_number] = {
                            line.id: line
                        }
        return parties_dict

    @classmethod
    @ModelView.button
    def create_certificate(cls, certificates):
        CpDocumentNumber = Pool().get('farming.cp_document_number')
        for record in certificates:
            cp_forms = []
            record.get_processed_lines()
            date_exp = cls.calculate_month()
            flimexp = date_exp.strftime("%Y-%m-%d")
            root = etree.Element("mas", nsmap={"xsi": "http://www.w3.org/2001/XMLSchema-instance"})
            root.set("{http://www.w3.org/2001/XMLSchema-instance}noNamespaceSchemaLocation", "../xsd/640.xsd")
            cab = etree.SubElement(root, "Cab")
            elements_to_add = {
                # "Ano": str(record.start_date.year),
                "Ano": str(2025),
                "CodCpt": "1",
                "Formato": "640",
                "Version": "1",
                "NumEnvio": record.number,
                "FecEnvio": str(datetime.today().isoformat()).split('.')[0],
                "FecInicial": str(date.today()),
                "FecFinal": str(date.today()),
                "ValorTotal": "31",
                "CantReg": str(len(record.suppliers))
            }
            for tag, text in elements_to_add.items():
                element = etree.Element(tag)
                element.text = text
                cab.append(element)
            for supplier in record.suppliers:
                invoice_lines = []
                vtcons = 0
                vtexen = 0
                # invoice_lines, vtcons, vtexen = cls.get_average_cost_product(record, supplier, record.invoice_lines)
                invoice_lines, vtcons, vtexen = cls.get_invoice_lines(record, supplier, record.invoice_lines)
                cp = etree.SubElement(root, "cp")
                cp.set("cpto", "1")
                cp.set("tdoc", "31")
                cp.set("ndoc", supplier.id_number)
                if supplier.type_document == '31' and supplier.type_person == 'persona_juridica':
                    cp.set("razsoc", supplier.name)
                else:
                    cp.set("apl1", supplier.first_family_name)
                    cp.set("apl2", supplier.second_family_name)
                    cp.set("nom1", supplier.first_name)
                    if supplier.second_name:
                        cp.set("nom2", supplier.second_name)
                # cp.set("cantfac", "1")
                cantfac = str(len(
                    set([inv_line.invoice.number for inv_line in record.invoice_lines if inv_line.invoice.party==supplier])))
                # cantfac = str(len(record.suppliers) if record.suppliers else 0)
                print(cantfac, 'cantfac')
                cp.set("cantfac", cantfac)
                cp.set("vtcons", f"{vtcons:.6f}")
                cp.set("vtexen", f"{vtexen:.6f}")
                cp.set("flimexp", flimexp)
                cp.set("nitems", str(len(invoice_lines)))
                cp_forms.append({
                    'certificate': record.id,
                    'supplier': supplier.id,
                    # 'amount': Decimal(str(round(vtcons, 4)))
                })
                for line in invoice_lines:
                    cphoja2 = etree.SubElement(cp, "cphoja2")
                    cphoja2.set("nfact", line['nfact'])
                    cphoja2.set("ffac", line['ffac'])
                    if 'resfac' in line:
                        cphoja2.set("resfac", line['resfac'])
                        cphoja2.set("fres", line['fres'])
                    cphoja2.set("tipo", '1')
                    cphoja2.set("subp", line['subp'])
                    cphoja2.set("desc", line['desc'])
                    cphoja2.set("cunfi", f"{line['cunfi']:.6f}")
                    cphoja2.set("unfi", line['unfi'])
                    cphoja2.set("cunco", f"{line['cunco']:.6f}")
                    cphoja2.set("unco", 'NAR')
                    cphoja2.set("vuni", f"{(line['amount']/line['cunfi']):.6f}")
                    cphoja2.set("vtotal", f"{line['amount']:.6f}")
                    cphoja2.set("tiva", '19.00')
                    cphoja2.set("vexen", f"{line['vexen']:.6f}")
                    cphoja2.set("codins", line['codins'])
            tree = etree.ElementTree(root)
            xml_certificate = etree.tostring(tree, xml_declaration=True, pretty_print=True, encoding="ISO-8859-1").decode("ISO-8859-1")
            record.xml_certificate = xml_certificate.replace("'", '"')
            record.save()
            CpDocumentNumber.create(cp_forms)
            # record.create_forms()

    def get_invoice_lines(self, supplier, lines):
        invoice_lines = []
        vtcons, vtexen = 0, 0
        start_date_auth = supplier.start_date_auth
        end_date_auth = supplier.end_date_auth
        if resolution_number := supplier.invoice_authorization_number:
            if not end_date_auth:
                raise UserError(f'{supplier.name} no tiene fecha de autorización')
            elif end_date_auth < lines[0].invoice.invoice_date:
                raise UserError(f'{supplier.name} tiene fecha de autorización vencida')
        for line in lines:
            if line.invoice.party == supplier:
                hts = line.product.harmonized_tariff_schedule
                if not hts:
                    raise UserError(f'El producto {line.product.name} no tiene código de tarifa arancelaria')
                amount = round(float(line.unit_price) * line.quantity, 6)
                vexen = round((float(line.unit_price) * line.quantity) * 0.19, 6)
                product_dict = {
                            'nfact': line.invoice.reference or line.invoice.number,
                            'ffac': str(line.invoice.invoice_date),
                            # 'resfac': resolution_number,
                            # 'fres': str(start_date_auth),
                            'tipo': '1',
                            'subp': hts.code,
                            'desc': hts.name,
                            # 'cunfi': f"{line.quantity:.6f}",
                            'cunfi': line.quantity,
                            'unfi': hts.uom.symbol,
                            # 'cunco': f"{line.quantity:.6f}",
                            'cunco': line.quantity,
                            'unco': 'NAR',
                            'vuni': line.unit_price,
                            'amount': amount,
                            'tiva': '19.00',
                            'vexen': vexen,
                            'codins': line.product.code,
                        }
                if resolution_number:
                    product_dict['resfac'] = resolution_number
                    product_dict['fres'] = str(start_date_auth)
                invoice_lines.append(product_dict)
                vtcons += amount
                vtexen += vexen
        if vtcons == 0:
            raise UserError(f'El tercero {supplier.name} no tiene lineas de factura')
        return invoice_lines, vtcons, vtexen

    def create_forms(self):
        if not self.cp_forms:
            cp_forms = []
            CpDocumentNumber = Pool().get('farming.cp_document_number')
            for supplier in self.suppliers:
                cp_forms.append({
                    'certificate': self.id,
                    'supplier': supplier
                })
            CpDocumentNumber.create(cp_forms)

    def get_average_cost_product(self, supplier, lines):
        invoice_line_dict = {}
        vtcons, vtexen = 0, 0
        # resolution_number = supplier.invoice_authorization_number
        start_date_auth = supplier.start_date_auth
        end_date_auth = supplier.end_date_auth
        if resolution_number := supplier.invoice_authorization_number:
            if not end_date_auth:
                raise UserError(f'{supplier.name} no tiene fecha de autorización')
            elif end_date_auth < lines[0].invoice.invoice_date:
                raise UserError(f'{supplier.name} tiene fecha de autorización vencida')
        for line in lines:
            amount = 0
            vexen = 0
            if line.invoice.party == supplier:
                hts = line.product.harmonized_tariff_schedule
                if not hts:
                    raise UserError(f'El producto {line.product.name} no tiene código de tarifa arancelaria')
                amount = round(float(line.unit_price) * line.quantity, 6)
                vexen = round((float(line.unit_price) * line.quantity) * 0.19, 6)
                if line.product.id not in invoice_line_dict:
                    invoice_line_dict[line.product.id] = {
                        'nfact': line.invoice.reference or line.invoice.number,
                        'ffac': str(line.invoice.invoice_date),
                        # 'resfac': resolution_number,
                        # 'fres': str(start_date_auth),
                        'tipo': '1',
                        'subp': hts.code,
                        'desc': hts.name,
                        # 'cunfi': f"{line.quantity:.6f}",
                        'cunfi': line.quantity,
                        'unfi': hts.uom.symbol,
                        # 'cunco': f"{line.quantity:.6f}",
                        'cunco': line.quantity,
                        'unco': 'NAR',
                        'vuni': line.unit_price,
                        'amount': amount,
                        'tiva': '19.00',
                        'vexen': vexen,
                        'codins': line.product.code,
                    }
                else:
                    invoice_line_dict[line.product.id]['amount'] += amount
                    invoice_line_dict[line.product.id]['vexen'] += vexen
                    invoice_line_dict[line.product.id]['cunfi'] += line.quantity
                    invoice_line_dict[line.product.id]['cunco'] += line.quantity
                if resolution_number:
                    invoice_line_dict[line.product.id]['resfac'] = resolution_number
                    invoice_line_dict[line.product.id]['fres'] = str(start_date_auth)
            vtcons += amount
            vtexen += vexen
        if vtcons == 0:
            raise UserError(f'El tercero {supplier.name} no tiene lineas de factura' )
        return invoice_line_dict.values(), vtcons, vtexen

    def get_processed_lines(self):
        pool = Pool()
        InvoiceLine = pool.get('farming.supplier_certificate-account_invoice_line')

        supplier_invoice_lines = [spl.id for spl in self.invoice_lines]
        invoice_lines = InvoiceLine.search([
            ('invoice_line', 'in', supplier_invoice_lines),
            ('supplier_certificate', '!=', self.id)
            ])
        if invoice_lines:
            for line in invoice_lines:
                if line.invoice_line.id in supplier_invoice_lines:
                    raise UserError(f"{line.invoice_line.product.name} de la linea de factura {line.invoice_line.invoice.number}")


class SupplierCetificateInvoiceLine(ModelSQL):
    'Supplier certificate - invoice Line'
    __name__ = 'farming.supplier_certificate-account_invoice_line'
    _table = 'supplier_certificate-account_invoice_line'
    supplier_certificate = fields.Many2One('farming.supplier_certificate',
        'Supplier Certificate', required=True, ondelete='CASCADE')
    invoice_line = fields.Many2One('account.invoice.line', 'Invoice Line',
        ondelete='CASCADE', required=True)

    # @classmethod
    # def __setup__(cls):
    #     super(SupplierCetificateInvoiceLine, cls).__setup__(s)
    #     table = cls.__table__()

    #     cls._sql_constraints += [
    #        ('invoice_line_uniq', Unique(table, table.invoice_line),
    #            f'Una linea de factura ya fue procesada'),
    #     ]


class SupplierCertificateDex(ModelView, ModelSQL):
    'Supplier Certificate Dex'
    __name__ = 'farming.supplier_certificate_dex'
    supplier_certificate = fields.Many2One('farming.supplier_certificate',
                                           'Supplier Certificate')
    dex = fields.Many2One('exportation.dex', 'Dex')
    invoice_line = fields.Many2One('account.invoice.line', 'Invoice Line')
    state = fields.Selection([
        ('partially_consumed', 'Partially Consumed'),
        ('consumed', 'Consumed'),
    ], 'State')


class CpDocumentNumber(ModelView, ModelSQL):
    "Cp Form"
    __name__ = 'farming.cp_document_number'

    certificate = fields.Many2One('farming.supplier_certificate', 'Certificate')
    supplier = fields.Many2One('party.party', 'Supplier',
                               states={'required': True})
    cp_form = fields.Char('Cp Form')
    # amount = fields.Numeric('Amount', digits=(16, 6))
    state = fields.Selection([
            ('active', 'Active'),
            ('cancelled', 'Cancelled')
        ], 'State', states={'readonly': True})
    invoice_lines = fields.Many2Many('farming.cp_document_number-account_invoice_line',
                                    'cp_document_number', 'invoice_line', 'Invoice Lines',
                                    states={'readonly': True})

    @staticmethod
    def default_state():
        return 'active'


# class DeleteCpInvoiceLine(ModelView, ModelSQL):
    # "Delete Cp Invoice Line"
    # __name__ = 'farming.delete_cp_invoice_line'
    # cp_document = fields.Many2One('farming.cp_document_number', 'Cp Document')
    # invoice_line = fields.Many2One('account.invoice.line', 'Invoice Line')


class DeleteCpInvoiceLine(ModelSQL):
    'Supplier certificate - invoice Line'
    __name__ = 'farming.cp_document_number-account_invoice_line'
    _table = 'cp_document_number-account_invoice_line'
    cp_document_number = fields.Many2One('farming.cp_document_number',
        'Cp Document Number', required=True, ondelete='CASCADE')
    invoice_line = fields.Many2One('account.invoice.line', 'Invoice Line',
        ondelete='CASCADE', required=True)


class CancelSupplierCertificateForm(Wizard):
    "Cancel Supplier Certificate Form"
    __name__ = 'farming.cancel_supplier_certificate_form'
    start_state = 'cancel_state'
    cancel_state = StateTransition()

    def transition_cancel_state(self):
        pool = Pool()
        id_ = Transaction().context['active_id']
        CpForm = pool.get('farming.cp_document_number')
        cp_form = CpForm(id_)
        Certificate = pool.get('farming.supplier_certificate')
        # DeleteCpInvoiceLine = pool.get('farming.delete_cp_invoice_line')
        CertificateInvoiceLine = pool.get('farming.supplier_certificate-account_invoice_line')
        certificate = Certificate(cp_form.certificate.id)
        invoice_line_ids = [inv_line.id
                            for inv_line in certificate.invoice_lines
                            if inv_line.invoice.party == cp_form.supplier]
        ceritificate_invoice_lines = CertificateInvoiceLine.search([('invoice_line', 'in', invoice_line_ids)])

        CertificateInvoiceLine.delete(ceritificate_invoice_lines)
        cp_form.invoice_lines = invoice_line_ids
        cp_form.state = 'cancelled'
        cp_form.save()
        return 'end'

    # certificate_state = fields.Function(
    #     fields.Selection('get_certificate_states', "Certificate State"),
    #     'on_change_with_certificate_state', searcher='search_certificate_state')

    # @fields.depends('certificate', '_parent_certificate.state')
    # def on_change_with_certificate_state(self, name=None):
    #     if self.certificate:
    #         return self.certificate.state

    # @classmethod
    # def search_certificate_state(cls, name, clause):
    #     return [('certificate.state', *clause[1:])]

    # @classmethod
    # def get_certificate_states(cls):
    #     pool = Pool()
    #     Sale = pool.get('farming.supplier_certificate')
    #     return Sale.fields_get(['state'])['state']['selection']


class SupplierCetificateParty(ModelSQL):
    'Supplier certificate - invoice Line'
    __name__ = 'farming.supplier_certificate-party.party'
    _table = 'supplier_certificate-party_party'
    supplier_certificate = fields.Many2One('farming.supplier_certificate',
        'Supplier Certificate', required=True, ondelete='CASCADE')
    party = fields.Many2One('party.party', 'Party',
        ondelete='CASCADE', required=True)


class PrintSupplierCertificateReport(Report):
    __name__ = 'farming.print_supplier_certificate.report'

    @classmethod
    def execute(cls, ids, data):
        SupplierCertificate = Pool().get('farming.supplier_certificate')
        result = super().execute(ids, data)
        consecutive = SupplierCertificate(ids[0]).number
        name_text = 'Dmuisca_010064001' + str(date.today().year) + consecutive.zfill(8)
        result = result[:3] + (name_text,)
        return result

    @classmethod
    def _execute(cls, groups, headers, data, action_report):
        oext, content = super()._execute(groups, headers, data, action_report)
        return 'xml', content.encode('ISO-8859-1')

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        report_context['xml_certificate'] = records[0].xml_certificate
        return report_context


class AnnualSupplierCertificationStart(ModelView):
    'Annual Supplier Certification Start'
    __name__ = 'farming.annual_supplier_certification.start'
    start_date = fields.Date("Start Date", required=True)
    end_date = fields.Date("End Date", required=True)

    @staticmethod
    def default_start_date():
        return date.today()

    @staticmethod
    def default_end_date():
        return date.today()


class AnnualSupplierCertification(Wizard):
    'Annual Supplier Certification'
    __name__ = 'farming.annual_supplier_certification'
    start = StateView(
        'farming.annual_supplier_certification.start',
        'farming.annual_supplier_certification_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('farming.annual_supplier_certification.report')

    def do_print_(self, action):
        data = {
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class AnnualSupplierCertificationReport(Report):
    __name__ = 'farming.annual_supplier_certification.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        SupplierCertificate = pool.get('farming.supplier_certificate')
        supplier_certificates = SupplierCertificate.search([
            ('start_date', '>=', data['start_date']),
            ('end_date', '<=', data['end_date']),
            ('state', '=', 'done')
            ], order=[('start_date', 'ASC')])
        # print(supplier_certificate)
        cp_lines = []
        for cp in supplier_certificates:
            cp_forms = {cp_form.supplier.id: cp_form.cp_form for cp_form in cp.cp_forms}
            for invoice_line in cp.invoice_lines:
                invoice = invoice_line.invoice
                cp_lines.append({
                    'form': cp_forms[invoice.party.id],
                    'amount': f"{invoice_line.amount:,.2f}",
                    'amount_': invoice_line.amount,
                    'date_form': cp.form_date.strftime("%d/%m/%y"),
                    'items': invoice_line.quantity,
                    'date_invoice': invoice.invoice_date.strftime("%d/%m/%y"),
                    'invoice_number': invoice.number
                })
            for cp_form_cacelled in cp.cp_forms:
                for invoice_line in cp_form_cacelled.invoice_lines:
                    invoice = invoice_line.invoice
                    cp_lines.append({
                        'form': cp_form_cacelled.cp_form,
                        'amount': f"{invoice_line.amount * -1:,.2f}",
                        'amount_': invoice_line.amount * (-1),
                        'date_form': cp.form_date.strftime("%d/%m/%y"),
                        'items': invoice_line.quantity,
                        'date_invoice': invoice.invoice_date.strftime("%d/%m/%y"),
                        'invoice_number': invoice.number
                    })
        report_context['total_amount'] = f"{sum(cpl['amount_'] for cpl in cp_lines):,.2f}"
        report_context['cp_lines'] = cp_lines
        return report_context
