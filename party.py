
from trytond.pool import PoolMeta
from trytond.model import fields, ModelSQL, ModelView
from trytond.transaction import Transaction


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    carrier = fields.Many2One('party.party', 'Carrier')
    freight_forwader = fields.Many2One('party.party', 'Freight Forwader')
    export_route = fields.Char('Export Route')
    customs_globals = fields.One2Many('party.customs_global', 'party',
        'Custom Globals')
    # supplier billing information
    invoice_authorization_number = fields.Char('Invoice Authorization Number')
    start_date_auth = fields.Date('Start Date Auth')
    end_date_auth = fields.Date('End Date Auth')
    airline_units = fields.One2Many('farming.airline_unit', 'party', 'Airline Units')


class Company(metaclass=PoolMeta):
    __name__ = 'company.company'
    ica_register = fields.Char('ICA Register')
    farm_code = fields.Char('Farm Code')


class CustomsGlobal(ModelSQL, ModelView):
    'Party Customs Global'
    __name__ = 'party.customs_global'
    _rec_name = 'global_custom'
    party = fields.Many2One('party.party', 'Party', ondelete='CASCADE',
        required=True)
    # export_target_city = fields.Many2One('party.city_code', 'Target City',
    #     required=True)
    global_custom = fields.Char('Global Custom')
    export_target_city = fields.Many2One('country.city', 'Target City',
        states={'required': True})


class AirlineUnit(ModelSQL, ModelView):
    "Airline"
    __name__ = 'farming.airline_unit'
    party = fields.Many2One('party.party', 'Party')
    code = fields.Char('Code')
    unit = fields.Many2One('product.uom', 'Unit')
