# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelView, ModelSQL
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.wizard import Wizard, StateTransition
from trytond.transaction import Transaction


class StockLot(metaclass=PoolMeta):
    __name__ = 'stock.lot'
    phyto = fields.Many2One("farming.phyto", "Phyto")
    phyto_move = fields.One2Many("stock.lot.phyto.move", 'lot',"Move Phyto")
    quantity_purchase = fields.Float('Quantity Purchase',
        digits=(16, Eval('unit_digits', 2)))
    arrival_date = fields.Date('Arrival Date')
    balance = fields.Float('Balance', digits=(16, Eval('unit_digits', 2)))
    balance_export = fields.Float('Balance Export', digits=(16,
        Eval('unit_digits', 2)))
    location = fields.Char('Location')

    @staticmethod
    def default_balance_export():
        return 0

    @classmethod
    def recompute_balance(cls, records):
        for rec in records:
            data = cls.get_balance(rec)
            rec.balance = data['balance']
            rec.balance_export = data['balance_export']
            rec.save()

    @fields.depends('phyto', 'number')
    def on_change_phyto(self):
        if self.phyto:
            self.number = self.phyto.ica.code + '-' + self.phyto.number

    @fields.depends('phyto_move', 'balance', 'balance_export')
    def on_change_phyto_move(self, name=None):
        data = self.get_balance(self)
        self.balance = data['balance']
        self.balance_export = data['balance_export']

    @classmethod
    def get_balance(cls, record):
        move_in = sum(m.move_in for m in record.phyto_move if m.move_in)
        move_out = sum(m.move_out for m in record.phyto_move if m.move_out)
        balance = record.quantity_purchase - move_in
        balance_export = move_in - move_out
        return {'balance': balance, 'balance_export': balance_export}


class PythoMove(ModelSQL, ModelView):
    'Phyto Move'
    __name__ = 'stock.lot.phyto.move'
    lot = fields.Many2One('stock.lot', 'Lot', ondelete='CASCADE')
    exportation_phyto = fields.Many2One(
        'exportation.phyto', 'Exportation Phyto')
    origin = fields.Reference('Origin', selection='get_origin')
    date_move = fields.Date("Date Move")
    move_in = fields.Float('Move In', digits=(16, Eval('unit_digits', 2)))
    move_out = fields.Float('Move out', digits=(16, Eval('unit_digits', 2)))

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return ['stock.move', 'exportation.phyto.line']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        get_name = Model.get_name
        models = cls._get_origin()
        return [(None, '')] + [(m, get_name(m)) for m in models]

    def get_rec_name(self, name=None):
        if self.lot:
            return self.lot.number

    @fields.depends('move_in', 'move_out')
    def on_change_move_in(self):
        self.move_out = self.move_in

    @classmethod
    def delete(cls, records):
        StockLot = Pool().get('stock.lot')
        stock_lots = set(r.lot.id for r in records)
        lots = StockLot.search([('id', 'in', list(stock_lots))])
        StockLot.recompute_balance(lots)
        super(PythoMove, cls).delete(records)


class StockMove(metaclass=PoolMeta):
    __name__ = 'stock.move'
    STATES = {
        'invisible': ~Eval('farming'),
    }
    farming = fields.Boolean('Farming')
    original_qty = fields.Float('Qty Original', states=STATES)
    production = fields.Many2One("production", "Production")
    style = fields.Many2One('product.style', 'Style')

    @classmethod
    def _get_origin(cls):
        return super(StockMove, cls)._get_origin() + [
            'production']
        # 'production', 'farming.crop.activity']


class ShipmentIn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'
    phyto = fields.Many2One('farming.phyto', 'Phyto', domain=[
        ('ica.party', '=', Eval('supplier'))]
    )

    @classmethod
    def done(cls, shipments):
        Lot = Pool().get('stock.lot')
        Move = Pool().get('stock.move')
        for shipment in shipments:
            lots = {}
            moves = []
            if shipment.phyto:
                number_lot = shipment.phyto.ica.code + '-' + shipment.phyto.number
                id_phyto = shipment.phyto.id
                for move in shipment.incoming_moves:
                    if not move.product.template.farming:
                        continue
                    lot, = Lot.create([{
                        'number' : number_lot,
                        'product' : move.product.id,
                        'phyto' : id_phyto,
                        'quantity_purchase' : move.quantity,
                        'balance' : move.quantity,
                        'arrival_date' : shipment.effective_date,
                    }])
                    lots[move.id] = lot.id
                for move in shipment.incoming_moves:
                    if move.id in list(lots.keys()):
                        move.lot = lots[move.id]
                        moves.append(move)
                Move.save(moves)
        super(ShipmentIn, cls).done(shipments)


class ShipmentOutProcess(Wizard):
    'Shipment Out Process'
    __name__ = 'stock.shipment.out.process'
    start_state = 'shipment_process'
    shipment_process = StateTransition()

    def transition_shipment_process(self):
        ids_ = Transaction().context['active_ids']
        Shipment = Pool().get('stock.shipment.out')
        for id_ in ids_:
            shipment = Shipment(id_)
            shipment.effective_date = shipment.planned_date
            shipment.save()
            Shipment.wait([shipment])
            Shipment.assign([shipment])
            Shipment.pick([shipment])
            Shipment.pack([shipment])
            Shipment.done([shipment])
        return 'end'
