
-- DROP TABLE farming_production_crop;
-- DROP TABLE farming_production_work;
-- DROP TABLE farming_production;
--
--
-- ALTER TABLE farming_crop_lot DROP COLUMN number;
-- ALTER TABLE farming_crop_lot_bed DROP COLUMN code;
-- ALTER TABLE farming_crop_stage DROP COLUMN activity_time;


-- DROP TABLE farming_variety_stage CASCADE;

-- ALTER TABLE farming_variety_cycle RENAME TO farming_variety_stage;
-- ALTER TABLE farming_crop_activity RENAME TO farming_crop_stage_activity;
-- ALTER TABLE farming_crop_activity_supply RENAME TO farming_crop_stage_activity_supply;


--before migrate
alter table sale_sale rename column export_target_city to export_target_city_old;
alter table exportation_charge_line rename column export_target_city to export_target_city_old;
alter table party_customs_global rename column export_target_city to export_target_city_old;

-- after migrate

SELECT cc.id, cc.name, pcc.id, pcc.name
FROM country_city AS cc
INNER JOIN party_city_code pcc
ON pcc.code=cc.dian_code
WHERE pcc.code=cc.dian_code;

UPDATE sale_sale SET export_target_city=a.city_new
FROM (SELECT cc.id AS city_new, cc.name, pcc.id AS city_old, pcc.name
FROM country_city AS cc
INNER JOIN party_city_code pcc
ON pcc.code=cc.dian_code
WHERE pcc.code=cc.dian_code) AS a
WHERE sale_sale.export_target_city_old=a.city_old;


UPDATE exportation_charge_line SET export_target_city=a.city_new
FROM (SELECT cc.id AS city_new, cc.name, pcc.id AS city_old, pcc.name
FROM country_city AS cc
INNER JOIN party_city_code pcc
ON pcc.code=cc.dian_code
WHERE pcc.code=cc.dian_code) AS a
WHERE exportation_charge_line.export_target_city_old=a.city_old;

UPDATE party_customs_global SET export_target_city=a.city_new
FROM (SELECT cc.id AS city_new, cc.name, pcc.id AS city_old, pcc.name
FROM country_city AS cc
INNER JOIN party_city_code pcc
ON pcc.code=cc.dian_code
WHERE pcc.code=cc.dian_code) AS a
WHERE party_customs_global.export_target_city_old=a.city_old;

ALTER TABLE "supplier_certificate-account_invoice_line"
DROP CONSTRAINT "supplier_certificate-account_invoice_line_invoice_line_uniq";

