from trytond.model import fields, ModelSQL, ModelView
from trytond.pyson import Eval


class ReturnSale(ModelSQL, ModelView):
    "Return Sale"
    __name__ = 'farming.sale_return'
    sale = fields.Many2One('sale.sale', 'Sale')
    reference = fields.Char('Reference')
    description = fields.Char('description')
    statement_line = fields.Many2One('account.statement.line',
                                     'Statement Line', domain=[
                                         ('sale', '=', Eval('sale')),
                                         ('type_line', '=', 'return')])
    lines = fields.One2Many('farming.sale_return.line', 'sale_return', 'Lines')


class ReturnSaleLine(ModelSQL, ModelView):
    "Return Sale Line"
    __name__ = 'farming.sale_return.line'
    sale_return = fields.Many2One('farming.sale_return', 'Return Sale')
    product = fields.Many2One('product.product', 'Product',
                              domain=[('farming', '=', True)])
    quantity = fields.Integer('Quantity')
