# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date, datetime, timedelta
from decimal import Decimal
from itertools import chain, groupby, product
import copy
from typing import OrderedDict

from sql import Table
from trytond.exceptions import UserError
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, And
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateTransition, StateView, Wizard
from trytond.i18n import gettext


STATES = {
    'readonly': Eval('state') == 'done',
}


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'
    carrier = fields.Many2One('party.party', 'Carrier', states=STATES)
    freight_forwader = fields.Many2One('party.party', 'Freight Forwader',
        states=STATES, help="The agent")
    mawb = fields.Char('MAWB', states=STATES)
    hawb = fields.Char('HAWB', states=STATES)
    charge = fields.Many2One('exportation.charge', 'Charge', states={
        'readonly': False,
    })
    # export_target_city = fields.Many2One('party.city_code',
    #     'Export Target City', required=True)
    export_target_city = fields.Many2One('country.city', 'Target City',
        states={'required': True})
    export_route = fields.Char('Export Route')
    boxes = fields.Function(fields.Float('Boxes', digits=(6, 2)),
        'get_lines_totals')
    unit_qty = fields.Function(fields.Float('Unit Qty', digits=(6, 0)),
        'get_lines_totals')
    packing_qty = fields.Function(fields.Float('Packing Qty', digits=(6, 0)),
        'get_lines_totals')
    quantity = fields.Function(fields.Float('Quantity', digits=(6, 0)),
        'get_lines_totals')
    aged = fields.Function(fields.Integer('Aged'), 'get_aged')
    total_amount_ref = fields.Function(fields.Numeric('Total Amount Ref.',
        digits=(6, 2)), 'get_total_amount_ref')
    custom_global = fields.Many2One('party.customs_global', 'Global',
        domain=[('party', '=', Eval('party'))])



    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls.shipping_date.states = {
            'readonly': Eval('state') == 'done',
        }
        cls.lines.states = {
            'readonly': Eval('state') == 'done',
        }

    @fields.depends('party', 'invoice_party', 'shipment_party',
        'payment_term', 'currency', 'carrier', 'freight_forwader')
    def on_change_party(self):
        super(Sale, self).on_change_party()
        if self.party:
            if self.party.currency:
                self.currency = self.party.currency.id
            if self.party.carrier:
                self.carrier = self.party.carrier.id
            if self.party.freight_forwader:
                self.freight_forwader = self.party.freight_forwader.id
            if self.party.addresses:
                city_co = self.party.addresses[0].city_co
                self.export_target_city = city_co.id if city_co else None
            if self.party.export_route:
                self.export_route = self.party.export_route

    @classmethod
    def _get_untaxed_amount(cls, sale):
        return sum(
            (line.amount for line in sale.lines
                if line.type == 'line'), Decimal(0),
            )

    def get_total_amount_ref(self, name=None):
        res = []
        for line in self.lines:
            if line.unit_price_ref:
                res.append(Decimal(line.quantity) * line.unit_price_ref)
        return sum(res)

    @classmethod
    def process(cls, sales):
        sale = sales[0]
        if sale.state != 'draft':
            sale.untaxed_amount_cache = cls._get_untaxed_amount(sale)
            sale.tax_amount_cache = sale.get_tax_amount()
            sale.total_amount_cache = sale.tax_amount_cache + sale.untaxed_amount_cache
            sale.save()
            super(Sale, cls).process(sales)
            for sale in sales:
                for line in sale.lines:
                    if not line.production and line.product.template.producible:
                        production_id = cls.create_production(line)
                        if production_id:
                            line.production = production_id
                            line.save()

    @classmethod
    def copy(cls, sales, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('shipping_date', None)
        return super(Sale, cls).copy(sales, default)

    @classmethod
    def create_production(cls, line):
        pool = Pool()
        Production = pool.get('production')
        Move = pool.get('stock.move')
        Bom = pool.get('production.bom')
        sale = line.sale
        if not sale.shipping_date:
            raise UserError('Falta la fecha de envio')
        planned_date = sale.shipping_date - timedelta(days=1)
        if planned_date < sale.sale_date:
            planned_date = sale.sale_date
        bom = Bom.search([('output_products', '=', line.product.id)])
        data = {
            'reference': sale.reference or sale.number,
            'customer': sale.party.id,
            'delivery_date': sale.shipping_date,
            'planned_date': sale.shipping_date,
            'planned_start_date': sale.shipping_date,
            'effective_date': sale.shipping_date,
            'effective_start_date': sale.shipping_date,
            'company': sale.company.id,
            'warehouse': sale.warehouse.id,
            'location': sale.warehouse.production_location.id,
            'product': line.product.id,
            'unit': line.packing_uom.id,
            'quantity': line.packing_qty,
            'state': 'waiting',
            'primary': True,
            'origin': str(line),
            'bom': bom[0].id if len(bom) > 0 else None,
            'factor_packing': line.factor_packing,
            'outputs': [('create', [{
                'product': line.product.id,
                'quantity': line.packing_qty,
                'unit': line.packing_uom.id,
                'unit_price': line.product.cost_price,
                'from_location': sale.warehouse.production_location.id,
                'to_location': sale.warehouse.storage_location.id,
                'currency': sale.currency.id
            }])],
        }
        _inputs = []
        subproductions = []
        for kit in line.kits:
            input = {
                'product': kit.product.id,
                'quantity': kit.quantity * line.packing_qty,
                'unit': kit.product.default_uom.id,
                'from_location': sale.warehouse.storage_location.id,
                'to_location': sale.warehouse.production_location.id,
                'style': kit.style.id if kit.style else None,
                'currency': sale.currency.id
            }
            if kit.product.producible:
                _production = cls._get_production(kit, sale, line)
                input['production_input'] = _production
                subproductions.append(_production)
            _inputs.append(input)
        # data['inputs'] = [('create', _inputs)]
        data['subproductions'] = [('add', subproductions)]
        production, = Production.create([data])
        production.explode_bom()
        production.save()
        for _input in _inputs:
            _input['production_input'] = production.id
        Move.create(_inputs)
        return production.id

    @classmethod
    def _get_production(cls, kit, sale, line):
        pool = Pool()
        Production = pool.get('production')
        Bom = pool.get('production.bom')
        Move = pool.get('stock.move')
        bom = Bom.search([('output_products', '=', kit.product)])
        data = {
            'reference': sale.number + ' | ' + line.summary,
            'company': sale.company.id,
            'customer': sale.party.id,
            'warehouse': sale.warehouse.id,
            'delivery_date': sale.shipping_date,
            'location': sale.warehouse.production_location.id,
            'planned_date': sale.shipping_date,
            'planned_start_date': sale.shipping_date,
            'effective_date': sale.shipping_date,
            'effective_start_date': sale.shipping_date,
            'product': kit.product.id,
            'unit': kit.product.default_uom.id,
            'quantity': kit.quantity * line.packing_qty,
            'state': 'waiting',
            # 'currency': sale.currency.id,
            'bom': bom[0].id if len(bom) > 0 else None,
            'origin': str(line),
            'outputs': [('create', [{
                'product': kit.product.id,
                'quantity': kit.quantity * line.packing_qty,
                'unit': kit.uom.id,
                'unit_price': kit.product.cost_price,
                'from_location': sale.warehouse.production_location.id,
                'to_location': sale.warehouse.storage_location.id,
                'currency': sale.currency.id
            }])],
            'state': 'waiting',
        }
        inputs = []
        production, = Production.create([data])
        production.explode_bom()
        production.save()
        for com in kit.components:
            inputs.append({
                'production_input': production.id,
                'product': com.product.id,
                'quantity': com.quantity * kit.quantity * line.packing_qty * com.product.default_uom.rate,
                'unit': com.product.default_uom.id,
                'from_location': sale.warehouse.storage_location.id,
                'to_location': sale.warehouse.production_location.id,
                'style': com.style.id if com.style else None,
                'currency': sale.currency.id
            })
        # data['inputs'] = [('create', inputs)]
        Move.create(inputs)
        return production.id

    def get_aged(self, name=None):
        if self.sale_date:
            aged = (date.today() - self.shipping_date).days
            return aged

    def get_lines_totals(self, name=None):
        return sum(getattr(line, name) or 0 for line in self.lines)


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'
    factor_packing = fields.Float('Factor Packing')
    reference = fields.Function(fields.Char('Reference'), 'get_reference')
    customer = fields.Function(fields.Many2One('party.party',
        'Customer'), 'get_customer')
    boxes = fields.Function(fields.Float('Boxes', digits=(6, 2)),
        'on_change_with_boxes')
    unit_qty = fields.Function(fields.Float('Unit Qty', digits=(10, 0)),
        'on_change_with_unit_qty')
    packing_uom = fields.Many2One('product.uom', 'Packing UoM')
    packing_qty = fields.Float('Packing Qty')
    kits = fields.One2Many('sale.line.kit', 'line', 'Line Kit')
    productions = fields.One2Many('production', 'origin', 'Productions')
    charge_line = fields.Many2One('exportation.charge.line', 'Charge Line')
    production = fields.Many2One('production', 'Production')
    quality_analysis = fields.One2Many('farming.quality.analysis', 'origin',
        'Quality Analysis', states=STATES)
    unit_price_ref = fields.Numeric('Price Ref.', digits=(6, 2))
    amount_ref = fields.Function(fields.Numeric('Amount Ref.',
        digits=(6, 2)), 'get_amount_ref')
    ica_register = fields.Many2One('farming.quality.ica', 'ICA Register')

    @classmethod
    def __setup__(cls):
        super(SaleLine, cls).__setup__()
        cls.unit_price.states = {
            'invisible': Eval('type') != 'line',
            'required': Eval('type') == 'line',
            'readonly': Eval('sale_state') == 'done',
        }

    @classmethod
    def copy(cls, lines, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('production', None)
        default.setdefault('charge_line', None)
        return super(SaleLine, cls).copy(lines, default)

    @fields.depends('quantity', 'unit')
    def on_change_with_unit_qty(self, name=None):
        if self.quantity and self.unit:
            res = self.quantity * self.unit.factor
            return res

    @fields.depends('packing_qty', 'factor_packing')
    def on_change_with_quantity(self):
        if self.packing_qty and self.factor_packing:
            return self.packing_qty * self.factor_packing

    @fields.depends('packing_qty', 'packing_uom')
    def on_change_with_boxes(self, name=None):
        if self.packing_qty and self.packing_uom:
            return self.packing_qty * self.packing_uom.factor

    @fields.depends('kits', 'product', 'description')
    def on_change_kits(self, name=None):
        if self.product and self.kits:
            string_ = [
                ki.product.template.name
                for ki in self.kits if ki.product and ki.product.template.farming]
            if not string_:
                return
            desc_full = ' | '.join([self.product.rec_name] + string_)
            self.description = desc_full

    def get_amount_ref(self, name=None):
        if self.unit_price_ref and self.quantity:
            return Decimal(self.quantity) * self.unit_price_ref

    def get_customer(self, name=None):
        if self.sale:
            return self.sale.party.id

    def get_reference(self, name=None):
        if self.sale:
            return self.sale.reference

    def _get_invoice_line_quantity(self):
        qty = self.quantity
        if self.sale.invoice_method != 'manual':
            qty = super(SaleLine, self)._get_invoice_line_quantity()
        return qty


class SaleLineKit(ModelSQL, ModelView):
    "Sale Line Kit"
    __name__ = "sale.line.kit"
    line = fields.Many2One('sale.line', 'Line', required=True,
        ondelete='CASCADE')
    product = fields.Many2One('product.product', 'Product', required=True,
        domain=[('type', '=', 'goods')])
    uom = fields.Many2One('product.uom', 'UoM')
    quantity = fields.Integer('Quantity', required=True)
    notes = fields.Char('Notes')
    components = fields.One2Many('sale.line.kit.component', 'kit', 'Components')
    style = fields.Many2One('product.style', 'Style')

    @fields.depends('product', 'uom')
    def on_change_with_uom(self, name=None):
        if self.product:
            return self.product.default_uom.id

    @staticmethod
    def default_quantity():
        return 1


class SaleLineKitComponent(ModelSQL, ModelView):
    "Sale Line Component"
    __name__ = "sale.line.kit.component"
    kit = fields.Many2One('sale.line.kit', 'Kit', ondelete='CASCADE',
        required=True)
    product = fields.Many2One('product.product', 'Product', required=True,
        domain=[('type', '=', 'goods')])
    quantity = fields.Float('Quantity', digits=(2, 2))
    notes = fields.Char('Notes')
    style = fields.Many2One('product.style', 'Style')

    @staticmethod
    def default_quantity():
        return 1


class SaleForceDraft(Wizard):
    __name__ = 'sale_co.force_draft'

    def transition_force_draft(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        Production = pool.get('production')
        Shipment = pool.get('stock.shipment.out')
        cursor = Transaction().connection.cursor()
        ids = Transaction().context['active_ids']
        if not ids:
            return 'end'
        sales = Sale.browse(ids)
        for sale in sales:
            for line in sale.lines:
                productions = Production.search([
                    ('origin', '=', str(line)),
                ])
                for pd in productions:
                    if pd.state in ['assigned', 'running', 'done']:
                        raise UserError(
                            'No se puede borrar la produccion porque esta en proceso',
                        )
                    else:
                        Production.delete([pd])

        super(SaleForceDraft, self).transition_force_draft()
        return 'end'


class GroupingSalesStart(ModelView):
    "Grouping Sales Start"
    __name__ = 'sale.grouping_sales.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    customer = fields.Many2One('party.party', 'Customer', required=True)
    currency = fields.Many2One('currency.currency', 'Currency', required=True)
    # to_currency = fields.Many2One('currency.currency', 'Currency')
    start_date = fields.Date("Start Date", required=True)
    end_date = fields.Date("End Date", required=True)
    sales = fields.Many2Many('sale.sale', None, None, 'Sales',
                             domain=['OR',
                                     [
                                        ('party', '=', Eval('customer')),
                                        ('invoice_party', '=', None),
                                        ('currency', '=', Eval('currency')),
                                        ('shipping_date', '>=', Eval('start_date')),
                                        ('shipping_date', '<=', Eval('end_date'))
                                        ],
                                    [
                                            ('invoice_party', '=', Eval('customer')),
                                            ('currency', '=', Eval('currency')),
                                            ('shipping_date', '>=', Eval('start_date')),
                                            ('shipping_date', '<=', Eval('end_date'))
                                        ]])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class GroupingSales(Wizard):
    "Grouping Sales"
    __name__ = 'sale.grouping_sales'
    start = StateView(
        'sale.grouping_sales.start',
        'farming.grouping_sales_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def _create_invoice(self, lines, sale):
        invoice_lines = []
        for line in lines:
            invoice_lines.append(line.get_invoice_line())
        invoice_lines = list(chain(*invoice_lines))
        if not invoice_lines:
            return

        invoice = sale._get_invoice_sale()
        if getattr(invoice, 'lines', None):
            invoice_lines = list(invoice.lines) + invoice_lines

        from_currency = sale.currency
        to_currency = invoice.company.currency

        if from_currency != to_currency:
            pool = Pool()
            Currency = pool.get('currency.currency')
            invoice.currency = to_currency
            invoice.second_currency = from_currency
            with Transaction().set_context(date=invoice.currency_date):
                for line in invoice_lines:
                    line.currency = to_currency
                    line.unit_price = Currency.compute(from_currency, line.unit_price, to_currency)
        invoice.lines = invoice_lines
        invoice.save()

        invoice.update_taxes()
        sale.copy_resources_to(invoice)
        return invoice

    def transition_accept(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        dom = None
        if self.start.sales:
            dom = [('id', 'in', self.start.sales)]
        else:
            dom = ['OR', [
                ('state', 'in', ['confirmed', 'processing', 'done']),
                ('invoice_state', '=', 'none'),
                ('party', '=', self.start.customer.id),
                ('invoice_party', '=', None),
                ('currency', '=', self.start.currency.id),
                ('shipping_date', '>=', self.start.start_date),
                ('shipping_date', '<=', self.start.end_date),
            ], [
                ('state', 'in', ['confirmed', 'processing', 'done']),
                ('invoice_state', '=', 'none'),
                ('invoice_party', '=', self.start.customer.id),
                ('currency', '=', self.start.currency.id),
                ('shipping_date', '>=', self.start.start_date),
                ('shipping_date', '<=', self.start.end_date),
            ]]
        sales = Sale.search(dom, order=[('shipping_date', 'ASC')])
        if not sales:
            return 'end'

        lines_to_bill = []
        for sale in sales:
            if sale.invoices:
                continue
            for line in sale.lines:
                lines_to_bill.append(line)
        if lines_to_bill:
            self._create_invoice(lines_to_bill, sales[0])
        return 'end'


class ReturnSale(metaclass=PoolMeta):
    __name__ = 'sale.return_sale'

    def do_return_(self, action):
        sales = self.records
        return_sales = self.model.copy(sales)
        for return_sale, sale in zip(return_sales, sales, strict=False):
            return_sale.origin = sale
            for line in return_sale.lines:
                line.production = None
                if line.type == 'line':
                    line.quantity *= -1
            return_sale.lines = return_sale.lines  # Force saving

        self.model.save(return_sales)

        data = {'res_id': [s.id for s in return_sales]}
        if len(return_sales) == 1:
            action['views'].reverse()
        return action, data


class SaleChangeProcessing(Wizard):
    __name__ = 'sale.change_processing'
    start_state = 'change_processing'
    change_processing = StateTransition()

    def transition_change_processing(self):
        Sale = Pool().get('sale.sale')
        sale_table = Table('sale_sale')
        cursor = Transaction().connection.cursor()
        sale_id = Transaction().context['active_id']
        if not sale_id:
            return 'end'

        sale, = Sale.browse([sale_id])
        if sale.state in ['done', 'processing'] and sale.id:
            cursor.execute(*sale_table.update(
                columns=[
                    sale_table.state,
                    sale_table.untaxed_amount_cache,
                    sale_table.tax_amount_cache,
                    sale_table.total_amount_cache],
                values=['processing', 0, 0, 0],
                where=sale_table.id == sale.id),
            )
        return 'end'


class PortfolioDetailedStart(ModelView):
    "Portfolio Detailed Start"
    __name__ = 'farming.portfolio_detailed.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    to_date = fields.Date('To Date')
    parties = fields.Many2Many('party.party', None, None, 'Parties')
    type = fields.Selection([
            ('out', 'Customer'),
        ], 'Type', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_type():
        return 'out'


class PortfolioDetailed(Wizard):
    "Portfolio Detailed"
    __name__ = 'farming.portfolio_detailed'
    start = StateView(
        'farming.portfolio_detailed.start',
        'farming.print_sale_portfolio_detailed_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('farming.portfolio_detailed.report')

    def do_print_(self, action):
        parties_ids = [p.id for p in self.start.parties]
        data = {
            'company': self.start.company.id,
            'type': self.start.type,
            'to_date': self.start.to_date,
            'parties': parties_ids,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class PortfolioDetailedReport(Report):
    __name__ = 'farming.portfolio_detailed.report'

    @classmethod
    def get_domain_inv(cls, dom_sales, data):
        dom_sales.append([
            ('company', '=', data['company']),
            ('state', 'not in', ['draft', 'cancelled']),
        ])

        if data['parties']:
            dom_sales.append(
                ('party', 'in', data['parties']),
            )

        if data['to_date']:
            dom_sales.append(
                ('shipping_date', '<=', data['to_date']),
            )
        return dom_sales

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)

        pool = Pool()
        Sale = pool.get('sale.sale')
        dom_sales = []
        dom_sales = cls.get_domain_inv(dom_sales, data)
        sales = Sale.search(
            dom_sales,
            order=[('party.name', 'ASC'), ('shipping_date', 'ASC')],
        )
        salesmans = {}
        for sale in sales:
            pay_to_date = []
            if data['to_date']:
                for line in sale.payments:
                    if line.date <= data['to_date']:
                        pay_to_date.append(line.amount)

                amount_to_pay = sale.total_amount - sum(pay_to_date)
            else:
                amount_to_pay = sale.residual_amount
                pay_to_date = [
                    pay.amount for pay in sale.payments if pay.amount]

            if sale.total_amount == sum(pay_to_date):
                continue

            if 'field_salesman' in data.keys() and \
                    data['field_salesman'] and sale.salesman:
                if sale.salesman.id not in salesmans:
                    salesmans[sale.salesman.id] = {
                        'salesman': sale.salesman.party.full_name,
                        'parties': {},
                        'total_invoices': [],
                        'total_amount_to_pay': [],
                    }

                salesman = sale.salesman.id
            elif 'without_seller' not in salesmans:
                salesmans['without_seller'] = {
                    'salesman': 'without_seller',
                    'parties': {},
                    'total_invoices': [],
                    'total_amount_to_pay': [],
                }
                salesman = 'without_seller'
            else:
                salesman = 'without_seller'
            if sale.party.id not in salesmans[salesman]['parties'].keys():
                salesmans[salesman]['parties'][sale.party.id] = {
                    'party': sale.party,
                    'invoices': [],
                    'total_invoices': [],
                    'total_amount_to_pay': [],
                }

            party_key = salesmans[salesman]['parties'][sale.party.id]
            salesmans[salesman]['total_invoices'].append(sale.total_amount)
            salesmans[salesman]['total_amount_to_pay'].append(amount_to_pay)
            party_key['invoices'].append(sale)
            party_key['total_invoices'].append(sale.total_amount)
            party_key['total_amount_to_pay'].append(amount_to_pay)

        report_context['records'] = salesmans
        report_context['data'] = data
        return report_context


class SalePaymentDetailedStart(ModelView):
    "Sale Portfolio Detailed Start"
    __name__ = 'farming.sale_portfolio_detailed.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    from_date = fields.Date('From Date')
    to_date = fields.Date('To Date')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class SalePortfolioDetailed(Wizard):
    "Sale Portfolio Detailed Wizard"
    __name__ = 'farming.sale_portfolio_detailed'
    start = StateView(
        'farming.sale_portfolio_detailed.start',
        'farming.sale_portfolio_detailed_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('farming.sale_portfolio_detailed.report')

    def do_print_(self, action):
        id_ = Transaction().context['active_id']
        data = {
            'company': self.start.company.id,
            'party': id_,
            'from_date': self.start.from_date,
            'to_date': self.start.to_date,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class SalePortfolioDetailedReport(Report):
    __name__ = 'farming.sale_portfolio_detailed.report'

    @classmethod
    def get_domain_inv(cls, data):
        dom_sales = []
        dom_sales.append([
            ('company', '=', data['company']),
            ('party', '=', data['party']),
            ('shipping_date', '>=', data['from_date']),
            ('shipping_date', '<=', data['to_date']),
            ('state', 'not in', ['draft', 'cancelled']),
        ])
        return dom_sales

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)

        pool = Pool()
        Company = pool.get('company.company')
        Party = pool.get('party.party')
        Sale = pool.get('sale.sale')
        dom_sales = cls.get_domain_inv(data)
        sales = Sale.search(
            dom_sales, order=[('shipping_date', 'ASC')],
        )
        # amount_to_pay = 0
        # for sale in sales:
        #     pay_to_date = []
        #     for line in sale.payments:
        #         print(line)
        #         pay_to_date.append(line.amount)
        #     amount_to_pay += sale.total_amount - sum(pay_to_date)
        # report_context['records'] = pay_to_date
        report_context['type_lines'] = {
            None: 'Pago',
            'commission': 'Comisión',
            'return': 'Devolución',
            'change': 'Cambio',
        }
        report_context['company'] = Company(data['company'])
        report_context['party'] = Party(data['party'])
        report_context['from_date'] = data['from_date']
        report_context['to_date'] = data['to_date']
        report_context['sales'] = sales
        report_context['invoice_model'] = pool.get('account.invoice')
        report_context['total_payment'] = cls.get_payment_values(sales)
        report_context['total_commission'] = cls.get_payment_values(sales, 'commission')
        report_context['total_change'] = cls.get_payment_values(sales, 'change')
        report_context['total_sale_return'] = cls.get_payment_values(sales, 'return')
        report_context['date_print'] = date.today()
        report_context['data'] = data
        return report_context

    @classmethod
    def get_payment_values(cls, sales, type_line=None):
        amount = 0
        for sale in sales:
            amount += sum(payment.amount for payment in sale.payments if payment.type_line == type_line)
        return amount


class AduanaDetailedStart(ModelView):
    "Portfolio Detailed Start"
    __name__ = 'farming.aduana_detailed.start'

    date = fields.Date('date')


class AduanaDetailed(Wizard):
    "Aduana Detailed"
    __name__ = 'farming.aduana_detailed'
    start = StateView(
        'farming.aduana_detailed.start',
        'farming.aduana_detailed_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('farming.aduana_detailed.report')

    def do_print_(self, action):
        data = {
            'date': self.start.date,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class AduanaDetailedReport(Report):
    __name__ = 'farming.aduana_detailed.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Sale = pool.get('sale.sale')
        sales = Sale.search(('shipping_date', '=', data['date']))
        report_sales = {}
        for sale in sales:
            if sale.number:
                total_weight = 0
                for line in sale.lines:
                    if line.packing_uom.symbol == 'EB':
                        total_weight = total_weight + (line.packing_qty * 3.1)
                    elif line.packing_uom.symbol == 'QB':
                        total_weight = total_weight + (line.packing_qty * 7)
                    elif line.packing_uom.symbol == 'HB':
                        total_weight = total_weight + (line.packing_qty * 8.5)

                report_sales[sale.number] = {
                    'client': sale.party.name,
                    'nit': sale.carrier.id_number,
                    'guia': sale.mawb,
                    'global': sale.custom_global and sale.custom_global.global_custom,
                    'invoice': sale.number,
                    'code': '0001',
                    'product': 'Hidrangea',
                    'units': sale.unit_qty,
                    'pieces': sale.packing_qty,
                    'weight': total_weight,
                    'total': sale.total_amount,
                    'license_plate': 'WHO-265',
                }
        report_context['records'] = report_sales.values()

        return report_context


class SaleCustomizeReport(Report):
    __name__ = 'farming.sale_customize.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        record_dict = {}
        reference_dicts = {}
        for record in records:
            record_dict[record.id] = {
                'sale': record,
                'reference_list': [],
            }
            for line in record.lines:
                try:
                    reference_dicts[line.product.reference]['products'].append(line)
                except KeyError:
                    reference_dicts[line.product.reference] = {
                        'reference': line.product.reference,
                        'products': [line],
                    }
            record_dict[record.id]['reference_list'] = reference_dicts.values()
        report_context['record_list'] = record_dict.values()
        report_context['company'] = Company(Transaction().context.get('company'))
        report_context['records'] = records
        return report_context


class SaleProductByCustomerStart(ModelView):
    'Sale Product By Customer Start'
    __name__ = 'farming.sale_product_by_customer.start'

    GROPING_METHODS = [
        (None, ''),
        ('general', 'Cliente General'),
        ('variety', 'Cliente Variedad'),
        ('carrier', 'Transportista'),
        ('main_product', 'Producto Principal'),
    ]

    customer = fields.Many2One('party.party', 'Customer')
    groping_method = fields.Selection(GROPING_METHODS, 'Groping Method')
    start_date = fields.Date('Start Date', states={'required': True})
    end_date = fields.Date('End Date', states={'required': True})


class SaleProductByCustomer(Wizard):
    'Sale Product By Customer'
    __name__ = 'farming.sale_product_by_customer'
    start = StateView(
        'farming.sale_product_by_customer.start',
        'farming.sale_product_by_customer_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('farming.sale_product_by_customer.report')

    def do_print_(self, action):
        data = {
            'customer': self.start.customer and self.start.customer.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'groping_method': self.start.groping_method
        }
        return action, data

    def transition_print_(self):
        return 'end'


class SaleProductByCustomerReport(Report):
    __name__ = 'farming.sale_product_by_customer.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        company_id = Transaction().context.get('company')
        company = pool.get('company.company')(company_id)
        Sale = pool.get('sale.sale')
        dom = [('sale_date', '>=', data['start_date']),
               ('sale_date', '<=', data['end_date']),]
        dom.append((('party', '=', data['customer']))) if data['customer'] else None
        field_names = [
            'currency.name', 'number', 'sale_date', 'party.name',
            'party.id_number', 'mawb', 'lines.unit_price', 'lines.boxes',
            'lines.packing_qty', 'lines.quantity', 'lines.kits.product',
            'lines.product.name', 'lines.kits.product', 'lines.kits.product.name',
            'lines.unit_price', 'lines.amount', 'lines.kits.product.template.farming',
            'lines.kits.quantity', 'lines.kits.components', 'carrier.name'
        ]
        if data['groping_method'] == 'carrier':
            sales = Sale.search_read(
                dom,
                fields_names=field_names,
                order=[('carrier.name', 'ASC')])
            records = cls.get_grouping_by_carrier(sales)
        elif data['groping_method'] == 'main_product':
            sales = Sale.search_read(dom, fields_names=field_names, order=[('party.name', 'ASC')])
            records = cls.get_by_main_product(sales)
        else:
            sales = Sale.search_read(
                dom,
                fields_names=field_names,
                order=[('party.name', 'ASC')])
            if data['groping_method'] == 'general':
                records = cls.get_grouping_by_customer_general(sales)
            else:
                records = cls.get_grouping_stem(sales) if data['groping_method'] == 'variety' else cls.get_detail_stems(sales)

        report_context['records'] = records
        report_context['groping_method'] = data['groping_method']
        report_context['company'] = company.rec_name
        report_context['start'] = data['start_date']
        report_context['end'] = data['end_date']
        return report_context

    @classmethod
    def get_by_main_product(cls, sales):
        sales_result = []
        farming_products = {}
        quantities = {
            'product': '',
            'quantity': 0,
            'amount': 0
        }
        for _, sale_group in groupby(sales, lambda x: x['party.']['name']):
            quantity= 0
            currency = None
            id_number = None
            amount = 0
            for sale in sale_group:
                currency = sale['currency.']['name']
                id_number = sale['party.']['id_number']
                for line in sale['lines.']:
                    # product = farming_products.get(line['product.']['id'], copy.deepcopy(quantities))
                    product_id = line['product.']['id']
                    product = farming_products.get(product_id)
                    if not product:
                        product = copy.deepcopy(quantities)
                        product['product'] = line['product.']['name']
                        farming_products[product_id] = product
                    product['quantity'] += line['quantity']
                    product['amount'] += line['amount']
                    quantity += line['quantity']
                    amount += line['amount']
            sales_result.append({
                'party': _,
                'id_number': id_number,
                'currency': currency,
                'products': farming_products,
                'quantity': quantity,
                'amount': amount
            })
            farming_products = {}
        return sales_result

    @classmethod
    def get_grouping_by_carrier(cls, sales):
        sales_result = []
        farming_products = {}
        quantities = {
            'product': '',
            'quantity': 0,
            'boxes': 0,
            'packing_qty': 0,
            'amount': 0
        }
        for _, sale_group in groupby(sales, lambda x: x['carrier.']['name']):
            boxes, quantity, packing_qty = 0, 0, 0
            currency = None
            amount = 0
            for sale in sale_group:
                currency = sale['currency.']['name']
                for line in sale['lines.']:
                    product_id = line['product.']['id']
                    product = farming_products.get(product_id)
                    if not product:
                        product = copy.deepcopy(quantities)
                        product['product'] = line['product.']['name']
                        farming_products[product_id] = product
                    product['quantity'] += line['quantity']
                    product['boxes'] += line['boxes']
                    product['packing_qty'] += line['packing_qty']
                    product['amount'] += line['amount']
                    quantity += line['quantity']
                    amount += line['amount']
                    packing_qty += line['packing_qty'] or 0
                    boxes += line['boxes'] or 0
            sales_result.append({
                'party': _,
                'currency': currency,
                'products': farming_products,
                'boxes': boxes,
                'quantity': quantity,
                'packing_qty': packing_qty,
                'amount': amount
            })
            farming_products = {}
        return sales_result

    @classmethod
    def get_grouping_by_customer_general(cls, sales):
        sales_result = []
        farming_products = {}
        quantities = {
            'product': '',
            'quantity': 0,
            'boxes': 0,
            'packing_qty': 0,
            'amount': 0
        }
        for _, sale_group in groupby(sales, lambda x: x['party.']['name']):
            boxes, quantity, packing_qty = 0, 0, 0
            currency = None
            id_number = None
            amount = 0
            for sale in sale_group:
                currency = sale['currency.']['name']
                id_number = sale['party.']['id_number']
                for line in sale['lines.']:
                    # product = farming_products.get(line['product.']['id'], copy.deepcopy(quantities))
                    product_id = line['product.']['id']
                    product = farming_products.get(product_id)
                    if not product:
                        product = copy.deepcopy(quantities)
                        product['product'] = line['product.']['name']
                        farming_products[product_id] = product
                    product['quantity'] += line['quantity']
                    product['boxes'] += line['boxes']
                    product['packing_qty'] += line['packing_qty']
                    product['amount'] += line['amount']
                    quantity += line['quantity']
                    amount += line['amount']
                    packing_qty += line['packing_qty'] or 0
                    boxes += line['boxes'] or 0
            sales_result.append({
                'party': _,
                'id_number': id_number,
                'currency': currency,
                'products': farming_products,
                'boxes': boxes,
                'quantity': quantity,
                'packing_qty': packing_qty,
                'amount': amount
            })
            farming_products = {}
        return sales_result

    @classmethod
    def get_grouping_stem(cls, sales):
        sales_result = []
        farming_products = {}
        Product = Pool().get('product.product')
        products = Product.search_read([('farming', '=', True)],
                                       fields_names=['name'])
        products_dict = {
            product['id']:
            {
                'name': product['name'],
                'boxes': 0, 'packing_qty': 0,
                'quantity': 0, 'unit_price': 0,
                'amount': 0,
                'unit_price': 0,
            }
            for product in products}
        for _, sale_group in groupby(sales, lambda x: x['party.']['name']):
            farming_products = copy.deepcopy(products_dict)
            boxes, quantity, packing_qty = 0, 0, 0
            currency = None
            id_number = None
            amount = 0
            for sale in sale_group:
                currency = sale['currency.']['name']
                id_number = sale['party.']['id_number']
                for line in sale['lines.']:
                    for kit in line['kits.']:
                        if kit['product.']['template.']['farming']:
                            farming_products[kit['product.']['id']]['quantity'] += kit['quantity']
                            farming_products[kit['product.']['id']]['boxes'] += line['boxes']
                            farming_products[kit['product.']['id']]['packing_qty'] += line['packing_qty']
                            farming_products[kit['product.']['id']]['amount'] += line['amount']
                            quantity += kit['quantity']
                            amount += line['amount']
                            packing_qty += line['packing_qty'] or 0
                            boxes += line['boxes'] or 0
            sales_result.append({
                'party': _,
                'id_number': id_number,
                'currency': currency,
                'products': list(filter(lambda x: x['quantity'] > 0, farming_products.values())),
                'boxes': boxes,
                'quantity': quantity,
                'packing_qty': packing_qty,
                'amount': amount
            })
        return sales_result

    @classmethod
    def get_detail_stems(cls, sales):
        farming_products = []
        for _, sale_group in groupby(sales, lambda x: x['party.']['name']):
            for sale in sale_group:
                mawb = sale['mawb']
                number = sale['number']
                currency = sale['currency.']['name']
                id_number = sale['party.']['id_number']
                for line in sale['lines.']:
                    for kit in line['kits.']:
                        if kit['product.']['template.']['farming']:
                            farming_products.append({
                                'party': _,
                                'id_number': id_number,
                                'number': number,
                                'mawb': mawb,
                                'product': kit['product.']['name'],
                                'currency': currency,
                                'unit_price': line['unit_price'],
                                'quantity': kit['quantity'],
                                'boxes': line['boxes'],
                                'packing_qty': line['packing_qty'],
                                'amount': round(line['unit_price'] * Decimal(kit['quantity']) * Decimal(line['packing_qty']), 2)
                            })
        return farming_products


# class SalePaymentForm(metaclass=PoolMeta):
#     __name__ = 'sale.payment.form'
#     type_line = fields.Selection([
#         (None, ''),
#         ('commission', 'Comisión'),
#         ('return', 'Devolución'),
#         ('change', 'Cambio'),
#     ], 'Types')


# class WizardSalePayment(metaclass=PoolMeta):
#     __name__ = 'sale.payment'

#     def transition_pay_(self):
#         pool = Pool()
#         Sale = pool.get('sale.sale')
#         StatementLine = pool.get('account.statement.line')
#         active_id = Transaction().context.get('active_id', False)
#         sale = Sale(active_id)
#         if self.start.do_invoice:
#             Sale.workflow_to_end([sale])
#         form = self.start
#         if form.amount_payable > 0:
#             if not sale.number:
#                 Sale.set_number([sale])

#             if not sale.party.account_receivable:
#                 raise UserError(
#                     gettext('sale_shop.msg_party_without_account_receivable', s=sale.party.name))
#             config = pool.get('sale.configuration')(1)
#             st_line = self.get_statement_line(sale, config)
#             if st_line:
#                 st_line['type_line'] = self.start.type_line
#                 line, = StatementLine.create([st_line])
#         return 'start' if sale.residual_amount != 0 else 'end'


#  class SelectMultiplePaymentSale(metaclass=PoolMeta):
#     __name__ = 'select_multiple_payment_sale'
#     type_line = fields.Selection([
#         (None, ''),
#         ('commission', 'Comisión'),
#         ('return', 'Devolución'),
#         ('change', 'Cambio'),
#     ], 'Types')


# class MultiplePaymentSale(metaclass=PoolMeta):
#     __name__ = 'multiple_payment_sale'

#     def transition_create_moves(self):
#         pool = Pool()
#         StatementLine = pool.get('account.statement.line')
#         statement = self.start.statement
#         description = self.start.description
#         party = self.start.party
#         account = party.account_receivable
#         for line in self.start.sales:
#             st_line = StatementLine(
#                     statement=statement,
#                     amount=line.amount_to_pay,
#                     party=party,
#                     sale=line.sale,
#                     account=account,
#                     description=description,
#                     date=date.today(),
#                     number=line.sale.number,
#                     type_line=line.type_line,
#                 )
#             st_line.save()
#         return 'end'

# class SalePaymentFlowersStart(ModelView):
#     "SalePaymentFlowersStart"
#     __name__ = 'farming.sale_payment_flowers.start'
#     statement = fields.Many2One('account.statement', 'Statement',
#        required=True, domain=[('state', '=', 'draft')])
#     from_date = fields.Date('From Date')
#     at_date = fields.Date('At Date')
#     party = fields.Many2One('party.party', 'Party', states={
#         'required': True,
#         'invisible': ~And(Eval('from_date'), Eval('at_date'))
#     })
#     date_pay = fields.Date('Date')
#     sales_to_pay = fields.One2Many('farming.sales_to_pay.ask', 'sale_payment_flower', 'Sales To Pay')

#     @fields.depends('party', 'from_date', 'at_date', 'date_pay')
#     def on_change_party(self):
#         pool = Pool()
#         Sale = pool.get('sale.sale')
#         sales = Sale.search([
#             ('state', 'not in', ['draft', 'cancelled']),
#             ('party', '=', self.party),
#             ('sale_date', '>=', self.from_date),
#             ('sale_date', '<=', self.at_date)
#         ])
#         date_pay = self.date_pay
#         SaleToPayAsk = pool.get('farming.sales_to_pay.ask')
#         sales_to_pay = []
#         for sale in sales:
#             if sale.residual_amount > 0:
#                 sales_to_pay.append(SaleToPayAsk(
#                         sale=sale,
#                         residual_amount=sale.residual_amount,
#                         date_pay=date_pay
#                     ))
#         # print(SaleToPayAsk.create(sales_to_pay))
#         self.sales_to_pay = sales_to_pay


# class SaleToPayAsk(ModelView):
#     "SaleToPayAsk"
#     __name__ = 'farming.sales_to_pay.ask'
#     sale_payment_flower = fields.Many2One('farming.sale_payment_flowers.start', 'sale_payment_flower')
#     sale = fields.Many2One('sale.sale', 'Sale', states={'readonly': True})
#     date_pay = fields.Date('Date')
#     payment = fields.Numeric('Payment', digits=(16, 2))
#     change = fields.Numeric('Change', digits=(16, 2))
#     commission = fields.Numeric('Commission', digits=(16, 2))
#     return_sale = fields.Numeric('Return Sale', digits=(16, 2))
#     residual_amount = fields.Numeric('Residual Amount', digits=(16, 2),
#                                      states={'readonly': True})

#     @fields.depends('sale', 'payment', 'change', 'commission', 'return_sale', 'residual_amount')
#     def on_change_payment(self):
#         if self.sale:
#             amount_to_pay = (self.payment or 0) + (self.change or 0) + (self.commission or 0) + (self.return_sale or 0)
#             self.residual_amount = self.sale.residual_amount - amount_to_pay

#     @fields.depends('sale', 'payment', 'change', 'commission', 'return_sale', 'residual_amount')
#     def on_change_change(self):
#         if self.sale:
#             amount_to_pay = (self.payment or 0) + (self.change or 0) + (self.commission or 0) + (self.return_sale or 0)
#             self.residual_amount = self.sale.residual_amount - amount_to_pay

#     @fields.depends('sale', 'payment', 'change', 'commission', 'return_sale', 'residual_amount')
#     def on_change_commission(self):
#         if self.sale:
#             amount_to_pay = (self.payment or 0) + (self.change or 0) + (self.commission or 0) + (self.return_sale or 0)
#             self.residual_amount = self.sale.residual_amount - amount_to_pay

#     @fields.depends('sale', 'payment', 'change', 'commission', 'return_sale', 'residual_amount')
#     def on_change_return_sale(self):
#         if self.sale:
#             amount_to_pay = (self.payment or 0) + (self.change or 0) + (self.commission or 0) + (self.return_sale or 0)
#             self.residual_amount = self.sale.residual_amount - amount_to_pay


# class SalePaymentFlowers(Wizard):
#     "Sale Payment Flowers"
#     __name__ = 'farming.sale_payment_flowers'
#     start = StateView(
#         'farming.sale_payment_flowers.start',
#         'farming.sale_payment_flowers_start_view_form', [
#             Button('Cancel', 'end', 'tryton-cancel'),
#             Button('Ok', 'accept', 'tryton-ok', default=True),
#         ])
#     accept = StateTransition()

#     def transition_create_moves(self, sale_to_pay, type_line, amount):
#         pool = Pool()
#         StatementLine = pool.get('account.statement.line')
#         statement = self.start.statement
#         party = self.start.party
#         account = party.account_receivable
#         if amount and amount > 0:
#             st_line = StatementLine(
#                     statement=statement,
#                     party=party,
#                     account=account,
#                     type_line=type_line,
#                     sale=sale_to_pay.sale,
#                     date=sale_to_pay.date_pay,
#                     number=sale_to_pay.sale.number,
#                     amount=amount
#                 )
#             st_line.save()

#     def transition_accept(self):
#         for line in self.start.sales_to_pay:
#             self.transition_create_moves(line, None, line.payment)
#             self.transition_create_moves(line, 'change', line.change)
#             self.transition_create_moves(line, 'commission', line.commission)
#             self.transition_create_moves(line, 'return', line.return_sale)
#         return 'end'
