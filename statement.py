from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.pool import Pool, PoolMeta

TYPES = [
    (None, ''),
    ('commission', 'Comisión'),
    ('return', 'Devolución'),
    ('change', 'Cambio'),
]


class StatementLine(metaclass=PoolMeta):
    __name__ = 'account.statement.line'
    type_line = fields.Selection(TYPES, 'Types')
